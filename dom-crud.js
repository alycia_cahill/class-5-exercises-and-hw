// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
const cta = document.createElement('a'); 
cta.appendChild(document.createTextNode('Buy Now!')); 
cta.setAttribute('id', 'cta'); 
const main = document.getElementsByTagName('main')[0]; 
main.appendChild(cta); 

// Access (read) the data-color attribute of the <img>,
// log to the console
const picture = document.getElementsByTagName('img')[0];
const pictureColor = picture.dataset.color; 
console.log(pictureColor); 

// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"
const liEls = document.getElementsByTagName('li')[2]; 
liEls.classList.add('hightlight'); 


// Remove (delete) the last paragraph
// (starts with "Available for purchase now…"
const getP = document.getElementsByTagName('p')[0]; 
const removeP = main.removeChild(getP); 
