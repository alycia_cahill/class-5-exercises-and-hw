// If an li element is clicked, toggle the class "done" on the <li>
const listItems = document.querySelectorAll('li'); 
//const listItem = document.getElementsByTagName ('li')[0]; 

const listItemToggle = function(e){
  this.classList.toggle("done"); 
};

for (let i=0; i<listItems.length; i++){
  listItems[i].addEventListener('click', listItemToggle); 
}

// If a delete link is clicked, delete the li element / remove from the DOM
const deleteLinks = document.querySelectorAll('.delete'); 
//const deleteButton = document.getElementsByClassName('delete')[0];

const deleteLi = function(e){
  parentLi = this.parentNode; 
  parentLi.remove(); 
};   

for (let b=0; b<deleteLinks.length; b++){
  deleteLinks[b].addEventListener('click', deleteLi); 
}

// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>
const todayList = document.getElementsByClassName('today-list')[0]; 
const laterList = document.getElementsByClassName('later-list')[0]; 

const moveBtn = document.querySelectorAll(".move"); 

const moveList = function(e){
  e.stopPropagation(); 
  parentLi = this.parentNode; 
  parentUL = parentLi.parentNode; 
  if (parentUL.className === "later-list"){
    todayList.appendChild(parentLi); 
    this.classList.remove("toToday"); 
    this.classList.add("toLater"); 
    this.innerText = "Move to Later"; 
  }else{
    laterList.appendChild(parentLi); 
    this.classList.remove("toLater"); 
    this.classList.add("toToday"); 
    this.innerText = "Move to Today"; 
  }

}

for (let i=0; i<moveBtn.length; i++){
  moveBtn[i].addEventListener('click', moveList); 
}


// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!  
// Make sure to add an event listener to your new <li> (if needed)
const addListItem = function(list, e) {
  e.preventDefault();
  const input = list.parentNode.getElementsByTagName('input')[0];
  const text = input.value; // use this text to create a new <li>
  const makeLi = document.createElement('LI'); 
  const makeToggle = makeLi.addEventListener('click', listItemToggle); 
  const makeSpan = document.createElement('SPAN'); 
  const createBtn  = document.createElement('A'); 
  const createDeleteBtn = createBtn.setAttribute('class','delete'); 
  const makeDelete = createBtn.addEventListener('click', deleteLi); 
  createBtn.innerHTML = 'Delete'; 
  const createMoveBtn = document.createElement('A'); 
  createMoveBtn.addEventListener('click', moveList);
  createMoveBtn.setAttribute('class', 'move'); 
  const makeText =  document.createTextNode(text); 
  const putTextinSpan = makeSpan.appendChild(makeText); 
  const theListItem = makeLi.appendChild(makeSpan); 
  makeLi.appendChild(createMoveBtn); 
  makeLi.appendChild(createBtn); 
  if (list.className==='today-list'){
    todayList.appendChild(makeLi); 
    createMoveBtn.classList.add('toLater'); 
    createMoveBtn.innerHTML = 'Move to Later'; 
  }else{
     laterList.appendChild(makeLi); 
      createMoveBtn.classList.add('toToday'); 
      createMoveBtn.innerHTML = 'Move toToday'; 
  }
  const clearText = input.value = ''; 
}

function addToToday(e) {
  return addListItem(todayList, e); 
}

function addLater(e){
  return addListItem(laterList, e); 
}

// Add this as a listener to the two Add links
let today = document.getElementsByClassName('today')[0]; 
let addTodaySection = today.getElementsByClassName('add')[0]; 
let addTodayItemBtn = addTodaySection.getElementsByTagName('a')[0];
let later = document.getElementsByClassName('later')[0]; 
let addLaterSection = later.getElementsByClassName('add')[0]; 
let addLaterItemBtn = addLaterSection.getElementsByTagName('a')[0]; 

addTodayItemBtn.addEventListener('click', addToToday ); 
addLaterItemBtn.addEventListener('click', addLater ); 

